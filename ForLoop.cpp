#include <iostream>
#include <cstdio>
using namespace std;

int main() {
    // Complete the code.
    int n1, n2;
    cin >> n1;
    cin >> n2;
    for (n1; n1 <= n2; n1++)
    {
        if (n1 >= 1 && n1 <= 9)
        {
            if (n1 == 1)
            {
                cout << "one" << endl;
            }
            if (n1 == 2)
            {
                cout << "two" << endl;
            }
            if (n1 == 3)
            {
                cout << "three" << endl;
            }
            if (n1 == 4)
            {
                cout << "four" << endl;
            }
            if (n1 == 5)
            {
                cout << "five" << endl;
            }
            if (n1 == 6)
            {
                cout << "six" << endl;
            }
            if (n1 == 7)
            {
                cout << "seven" << endl;
            }
            if (n1 == 8)
            {
                cout << "eight" << endl;
            }
            if (n1 == 9)
            {
                cout << "nine" << endl;
            }
        }
        else if (n1 > 9)
        {
            if (n1 % 2 == 0)
            {
                cout << "even" << endl;
            }
            else
                cout << "odd" << endl;
        }
    }


    return 0;
}